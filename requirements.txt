asgiref==3.5.2
Django==4.1.2
djangorestframework==3.14.0
psycopg2-binary
pytz==2022.5
sqlparse==0.4.3
django-environ==0.9.0
