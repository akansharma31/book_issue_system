# Pull base image
FROM --platform=linux/amd64 python:3.10.2-slim-bullseye

# Set environment variables
ENV PIP_DISABLE_PIP_VERSION_CHECK 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set work directory
WORKDIR /book_issue_system

# Install dependencies
COPY ./requirements.txt .
RUN pip install -r requirements.txt

# Copy project
COPY . .

ADD init.sh /usr/src/app/init.sh
USER root
RUN chmod +x /usr/src/app/init.sh

ENTRYPOINT ["sh","/usr/src/app/init.sh"]