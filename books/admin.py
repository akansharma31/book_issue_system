from django.contrib import admin
from books.models import Book, BookCategoryDetail, Category, Section
# Register your models here.

admin.site.register(Book)
admin.site.register(Category)
admin.site.register(BookCategoryDetail)
admin.site.register(Section)
