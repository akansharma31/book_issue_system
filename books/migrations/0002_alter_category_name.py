# Generated by Django 4.1.2 on 2022-10-28 08:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(choices=[('Fiction', 'Fiction'), ('Horror', 'Horror'), ('Adventure', 'Adventure'), ('Quest', 'Quest'), ('Non-Fiction', 'Non-Fiction'), ('Sci-fi', 'Sci-Fi'), ('Mystery', 'Mystery'), ('Thriller', 'Thriller'), ('Autobiography', 'Autobiography'), ('Biography', 'Biography')], max_length=100),
        ),
    ]
