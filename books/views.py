from rest_framework import viewsets
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from rest_framework.response import Response
from books.models import Book, Category, BookCategoryDetail
from books.serializers import BookSerializer, CategorySerializer, UserSerializer, UserBooksSerializer

# Create your views here.

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    
class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

class UserBooksViewset(viewsets.ViewSet):
    def retrieve(self, request, pk=None):
        books = Book.objects.filter(issued_to=pk)
        serializer = UserBooksSerializer({'books' : books})
        return Response(serializer.data)

    def create(self, request, pk):
        serializer = UserBooksSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer)