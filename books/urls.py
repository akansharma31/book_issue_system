from django.urls import path, include
from books import views

app_name = 'books'

urlpatterns = [
    path('users/', views.UserViewSet.as_view({'get':'list', 'post':'create'}), name='users'),
    path('users/<int:pk>/', views.UserViewSet.as_view({'get':'retrieve', 'put':'update', 'delete' : 'destroy'})),
    path('books/', views.BookViewSet.as_view({'get':'list', 'post': 'create'})),
    path('books/<int:pk>/', views.BookViewSet.as_view({'get':'retrieve', 'put':'update', 'delete' : 'destroy'})),
    path('categories/', views.CategoryViewSet.as_view({'get':'list', 'post':'create'})),
    path('categories/<int:pk>/', views.CategoryViewSet.as_view({'get':'retrieve', 'put':'update', 'delete' : 'destroy'})),
    path('users/<int:pk>/books/', views.UserBooksViewset.as_view({'get':'retrieve', 'post':'create'})),
]