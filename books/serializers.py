from rest_framework import serializers
from django.contrib.auth.models import User
from  books.models import Book, Category


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username']


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'

class UserBooksSerializer(serializers.Serializer):
    books = BookSerializer(many=True)

    def create(self, validated_data):
        books_data = validated_data.pop('books')
        for book_data in books_data:
            Book.objects.create(**book_data)
        return books_data