from rest_framework.test import APIClient
from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model
from rest_framework import status
CREATE_USER_URL = reverse('books:users')

class UserAPITest(TestCase):
    def setUp(self) -> None:
       self.client = APIClient()

    def test_create_user_successful(self):
        data = {
            "email": 'test@gmail.com',
            'password': "test@123",
            'username': "name"
        }
        res = self.client.post(CREATE_USER_URL, data)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

    def test_user_with_email_exists_error(self):
        data = {
            'username' : 'user',
            "email": 'test@gmail.com',
        }
        user = get_user_model().objects.create_user(**data)

        data = {
            "email": 'test@gmail.com',
        }

        res = self.client.post(CREATE_USER_URL, data)

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
    
    def test_password_too_short_error(self):
        with self.assertRaises(Exception):
            data = {
                "email": 'test@gmail.com',
                'password': 'qw'
            }
            user = get_user_model().objects.create_user(**data)
            
    def test_empty_fields(self):
        with self.assertRaises(Exception):
            data = {
            }
            user = get_user_model().objects.create_user(**data)
    
    def test_new_user_without_email_raises_error(self):
        with self.assertRaises(Exception):
            data = {
                'email': '',
                'password': 'qwert'
            }
            user = get_user_model().objects.create_user(**data)