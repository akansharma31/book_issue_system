from django.db import models

CHOICES = [
    ('Fiction', 'Fiction'),
    ('Horror', 'Horror'),
    ('Adventure', 'Adventure'),
    ('Quest', 'Quest'),
    ('Non-Fiction', 'Non-Fiction'),
    ('Sci-fi', 'Sci-Fi'),
    ('Mystery','Mystery'),
    ('Thriller','Thriller'),
    ('Autobiography', 'Autobiography'),
    ('Biography', 'Biography'),
]

class Category(models.Model):
    name = models.CharField(choices=CHOICES, max_length=100)

    def __str__(self):
        return f'{self.name}'