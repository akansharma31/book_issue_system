from django.db import models
from books.models import Section
from django.contrib.auth.models import User

class Book(models.Model):
    name = models.CharField(max_length=50, unique=True)
    price = models.BigIntegerField()
    issued = models.BooleanField(default=False)
    section = models.ForeignKey(Section, on_delete=models.DO_NOTHING, null=True)
    issued_to = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True)

    def __str__(self):
        return f'{self.name}'