from django.db import models
from books.models import Book, Category

class BookCategoryDetail(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    class Meta:
        unique_together = (("book", "category"),)

    def __str__(self):
        return f'{self.book} - {self.category}'